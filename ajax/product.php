<?php

require_once 'config.php';


// The mysql database connection script

// a query get all the records from the users table
$sql = 'SELECT * FROM products WHERE id = :id';

// use prepared statements, even if not strictly required is good practice
$stmt = $dbh->prepare( $sql );
$stmt->execute(array(':id'=>$_GET['id']));


// fetch the results into an array
$result = $stmt->fetchAll( PDO::FETCH_ASSOC );

// convert to json
$json = json_encode( $result );

// echo the json string
echo $json;


?>