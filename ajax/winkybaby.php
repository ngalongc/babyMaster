<?php

require_once 'config.php';

if ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['submit'] == "Add") {
    try{
        // a query delete the records from the users table
        $sql = 'INSERT INTO products (id, imageUrl, brandName, name, snippet, price, color, description) 
                VALUES (:id, :imageUrl, :brandName, :name, :snippet, :price, :color, :description)';
    
        // use prepared statements, even if not strictly required is good practice
        $stmt = $dbh->prepare( $sql );
        $stmt->execute(array(':id'=>$_POST['id-create'], ':imageUrl'=>$_POST['imageUrl-create'], 
                            ':brandName'=>$_POST['brandName-create'], ':name'=>$_POST['name-create'],
                            ':snippet'=>$_POST['snippet-create'], ':price'=>$_POST['price-create'], 
                            ':color'=>$_POST['color-create'], ':description'=>$_POST['description-create']));

    }catch(Exception $e){

        echo $e->getMessage();

    }


}elseif ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['submit'] == "Delete") {
    try{
        $sql = 'DELETE FROM products WHERE id = :id';
        // use prepared statements, even if not strictly required is good practice
        $stmt = $dbh->prepare( $sql );
        $stmt->execute(array(':id'=>$_POST['id-delete']));
        echo "I am going to delete". $_POST['id-delete'];
    }catch(Exception $e){

        echo $e->getMessage();

    }
}elseif ($_SERVER["REQUEST_METHOD"] == "POST" && $_POST['submit'] == "Edit") {
    try{

        $id = $_POST['id-edit'];
        $imageUrl = $_POST['imageUrl-edit'];
        $brandName = $_POST['brandName-edit'];
        $name = $_POST['name-edit'];
        $snippet = $_POST['snippet-edit'];
        $price = $_POST['price-edit'];
        $description = $_POST['description-edit'];
        $color = $_POST['color-edit'];

        $sql = 'UPDATE products SET imageUrl=:imageUrl,
            brandName=:brandName, name=:name,
            snippet=:snippet, price=:price,
            description=:description, color=:color WHERE id=:id';
        $stmt = $dbh->prepare( $sql );
        $stmt->execute(array(':id'=>$id, ':imageUrl'=>$imageUrl, ':brandName'=>$brandName, 
            ':name'=>$name, ':snippet'=>$snippet, ':price'=>$price, ':description'=>$description,
            ':color'=>$color));
        echo $id, $imageUrl, $brandName, $name, $snippet, $price, $description, $color;
        
    }catch(Exception $e){
        echo $e->getMessage();
    }

}else{

    echo "False Connect!";

};

?>
