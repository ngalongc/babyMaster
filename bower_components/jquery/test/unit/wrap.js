(function() {

if ( !jQuery.fn.wrap ) { // no wrap module
	return;
}

module( "wrap", {
	teardown: moduleTeardown
});

// See test/unit/manipulation.js for explanation about these 2 functions
function manipulationBareObj( value ) {
	return value;
}

function manipulationFunctionReturningObj( value ) {
	return function() {
		return value;
	};
}

function testWrap( val ) {

	expect( 19 );

	var defaultText, result, j, i, cacheLength;

	defaultText = "Try them out:";
	result = jQuery("#first").wrap( val("<div class='red'><span></span></div>") ).text();

	equal( defaultText, result, "Check for wrapping of on-the-fly html" );
	ok( jQuery("#first").parent().parent().is(".red"), "Check if wrapper has class 'red'" );

	QUnit.reset();
	result = jQuery("#first").wrap( val(document.getElementById("empty")) ).parent();
	ok( result.is("ol"), "Check for element wrapping" );
	equal( result.text(), defaultText, "Check for element wrapping" );

	QUnit.reset();
	jQuery("#check1").on( "click", function() {
		var checkbox = this;

		ok( checkbox.checked, "Checkbox's state is erased after wrap() action, see #769" );
		jQuery( checkbox ).wrap( val("<div id='c1' style='display:none;'></div>") );
		ok( checkbox.checked, "Checkbox's state is erased after wrap() action, see #769" );
	}).prop( "checked", false )[ 0 ].click();

	// using contents will get comments regular, text, and comment nodes
	j = jQuery("#nonnodes").contents();
	j.wrap( val("<i></i>") );

	// Blackberry 4.6 doesn't maintain comments in the DOM
	equal( jQuery("#nonnodes > i").length, jQuery("#nonnodes")[ 0 ].childNodes.length, "Check node,textnode,comment wraps ok" );
	equal( jQuery("#nonnodes > i").text(), j.text(), "Check node,textnode,comment wraps doesn't hurt text" );

	// Try wrapping a disconnected node
	cacheLength = 0;
	for ( i in jQuery.cache ) {
		cacheLength++;
	}

	j = jQuery("<label/>").wrap( val("<li/>") );
	equal( j[ 0 ] .nodeName.toUpperCase(), "LABEL", "Element is a label" );
	equal( j[ 0 ].parentNode.nodeName.toUpperCase(), "LI", "Element has been wrapped" );

	for ( i in jQuery.cache ) {
		cacheLength--;
	}
	equal( cacheLength, 0, "No memory leak in jQuery.cache (bug #7165)" );

	// Wrap an element containing a text node
	j = jQuery("<span/>").wrap("<div>test</div>");
	equal( j[ 0 ].previousSibling.nodeType, 3, "Make sure the previous node is a text element" );
	equal( j[ 0 ].parentNode.nodeName.toUpperCase(), "DIV", "And that we're in the div element." );

	// Try to wrap an element with multiple elements (should fail)
	j = jQuery("<div><span></span></div>").children().wrap("<p></p><div></div>");
	equal( j[ 0 ].parentNode.parentNode.childNodes.length, 1, "There should only be one element wrapping." );
	equal( j.length, 1, "There should only be one element (no cloning)." );
	equal( j[ 0 ].parentNode.nodeName.toUpperCase(), "P", "The span should be in the paragraph." );

	// Wrap an element with a jQuery set
	j = jQuery("<span/>").wrap( jQuery("<div></div>") );
	equal( j[ 0 ].parentNode.nodeName.toLowerCase(), "div", "Wrapping works." );

	// Wrap an element with a jQuery set and event
	result = jQuery("<div></div>").on( "click", function() {
		ok( true, "Event triggered." );

		// Remove handlers on detached elements
		result.off();
		jQuery(this).off();
	});

	j = jQuery("<span/>").wrap( result );
	equal( j[ 0 ].parentNode.nodeName.toLowerCase(), "div", "Wrapping works." );

	j.parent().trigger("click");

	// clean up attached elements
	QUnit.reset();
}

test( "wrap(String|Element)", function() {
	testWrap( manipulationBareObj );
});

test( "wrap(Function)", function() {
	testWrap( manipulationFunctionReturningObj );
});

test( "wrap(Function) with index (#10177)", function() {
	var expectedIndex = 0,
		targets = jQuery("#qunit-fixture p");

	expect( targets.length );
	targets.wrap(function(i) {
		equal( i, expectedIndex, "Check if the provided index (" + i + ") is as expected (" + expectedIndex + ")" );
		expectedIndex++;

		return "<div id='wrap_index_'" + i + "'></div>";
	});
});

test( "wrap(String) consecutive elements (#10177)", function() {
	var targets = jQuery("#qunit-fixture p");

	expect( targets.length * 2 );
	targets.wrap("<div class='wrapper'></div>");

	targets.each(function() {
		var $this = jQuery(this);

		ok( $this.parent().is(".wrapper"), "Check each elements parent is correct (.wrapper)" );
		equal( $this.siblings().length, 0, "Each element should be wrapped individually" );
	});
});

function testWrapAll( val ) {

	expect( 8 );

	var prev, p, result;

	prev = jQuery("#firstp")[ 0 ].previousSibling;
	p = jQuery("#firstp,#first")[ 0 ].parentNode;
	result = jQuery("#firstp,#first").wrapAll( val("<div class='red'><div class='tmp'></div></div>") );

	equal( result.parent().length, 1, "Check for wrapping of on-the-fly html" );
	ok( jQuery("#first").parent().parent().is(".red"), "Check if wrapper has class 'red'" );
	ok( jQuery("#firstp").parent().parent().is(".red"), "Check if wrapper has class 'red'" );
	equal( jQuery("#first").parent().parent()[ 0 ].previousSibling, prev, "Correct Previous Sibling" );
	equal( jQuery("#first").parent().parent()[ 0 ].parentNode, p, "Correct Parent" );

	QUnit.reset();
	prev = jQuery("#firstp")[ 0 ].previousSibling;
	p = jQuery("#first")[ 0 ].parentNode;
	jQuery("#firstp,#first").wrapAll( val(document.getElementById("empty")) );

	equal( jQuery("#first").parent()[ 0 ], jQuery("#firstp").parent()[ 0 ], "Same Parent" );
	equal( jQuery("#first").parent()[ 0 ].previousSibling, prev, "Correct Previous Sibling" );
	equal( jQuery("#first").parent()[ 0 ].parentNode, p, "Correct Parent" );
}

test( "wrapAll(String|Eleme