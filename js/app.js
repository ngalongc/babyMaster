'use strict';

/* App Module */
var babyApp = angular.module('babyApp',[
  'ngRoute',
  'babyControllers'
]);


babyApp.config(['$routeProvider',
  function($routeProvider){
    $routeProvider.
      when('/products',{
         templateUrl: "partials/product-list.html",
         controller: "ProductListCtrl"
     }).
     when('/products/:productId', {
         templateUrl: "partials/product-detail.html",
         controller: "ProductDetailCtrl"
     }).
     when('/about-us',{
        templateUrl: "partials/about-us.html"
     }).
     when('/admin',{
        templateUrl: "partials/admin.html",
        controller: "ProductListCtrl"
     }).
     otherwise({
         redirectTo: "/products"
     });
 }]);