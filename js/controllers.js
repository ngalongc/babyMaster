'use strict';

/* Controllers */

var babyControllers = angular.module('babyControllers', []);


babyControllers.controller('ProductListCtrl', ['$scope', '$http',
  function($scope, $http){
   $http.get('../ajax/products.php').success(function(data){
     $scope.products = data;
   });

   $scope.orderProp = 'price';
   $scope.brandName = "";
 }]);

babyControllers.controller('ProductDetailCtrl', ['$scope', '$routeParams', '$http',
  function($scope, $routeParams, $http){
   $http.get('../ajax/product.php?id=' + $routeParams.productId).success(function(data){
     $scope.details = data;
   });

 }]);