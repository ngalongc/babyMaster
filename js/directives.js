'use strict';

/* Directives */

babyApp.directive('babyBrandName', function () {
    return {
    	restrict: "EA",
    	template: '<label>All<input type="radio" ng-model="brandName" value=""></label><br/>'+
    '<label>Urban Decay<input type="radio" ng-model="brandName" value="urban"/></label><br/>'+
    '<label>Bare Minerals<input type="radio" ng-model="brandName" value="bareminerals"></label><br/>'+
    '<label>First Aid Beauty<input type="radio" ng-model="brandName" value="First Aid Beauty"></label><br/>'+
    '<label>Too Faced<input type="radio" ng-model="brandName" value="Too Faced"></label><br/>'+
    '<label>Benefit Cosmetics<input type="radio" ng-model="brandName" value="Benefit Cosmetics"></label><br/>'+
    '<label>Tarte<input type="radio" ng-model="brandName" value="Tarte"></label><br/>'+
    '<label>theBalm<input type="radio" ng-model="brandName" value="theBalm"></label><br/>'+
    '<label>Fresh<input type="radio" ng-model="brandName" value="Fresh"></label><br/>'+
    '<label>Sale<input type="radio" ng-model="brandName" value="sale"></label><br/>'
    };
});
