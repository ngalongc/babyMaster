import json

description = {"description": ""}
products = open('products.json')

file_name = json.load(products)
for item in file_name:
	product_read_only = open(item['id']+'.json')
	append_data = json.load(product_read_only)
	product_read_only.close()
	append_data.update(description)
	product_write_read_tgt = open(item['id']+'.json', 'w')
	json.dump(append_data, product_write_read_tgt, indent=4)
	product_write_read_tgt.close()

products.close()