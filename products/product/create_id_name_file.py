import json
json_data = open('products.json')

data = json.load(json_data)
for item in data:
	file = open(item['id']+'.json', 'w+')
	json.dump(item, file, indent=4, sort_keys=True)
	file.close()
	json_data.close()
